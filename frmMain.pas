unit frmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, IniFiles, StrUtils, System.SyncObjs, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, dxCore;

type
  TMyThread = class(TThread)
  private
    FInterval: Integer;
    FWaitEvent: TEvent;
    procedure UpdateData();
  protected
    procedure Execute; override;
    procedure TerminatedSet; override;
  public
    constructor Create(Interval: Cardinal; CreateSuspended: Boolean);
    destructor Destroy; override;
end;

type
  TMain = class(TForm)
    Panel1: TPanel;
    btnExecute: TButton;
    dsData: TDataSource;
    btnSave: TButton;
    btnRestore: TButton;
    cxView: TcxGridDBTableView;
    cxLevel: TcxGridLevel;
    cxGrid: TcxGrid;
    cxViewsymbol: TcxGridDBColumn;
    cxViewhigh: TcxGridDBColumn;
    cxViewlow: TcxGridDBColumn;
    cxViewvolume: TcxGridDBColumn;
    cxViewquoteVolume: TcxGridDBColumn;
    cxViewpercentChange: TcxGridDBColumn;
    cxViewupdatedAt: TcxGridDBColumn;
    cxSymbol1: TcxGridDBColumn;
    cxSymbol2: TcxGridDBColumn;
    procedure btnExecuteClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnRestoreClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    mt: TMyThread;
    function GetColumn(aFieldName : string): TcxGridDBColumn;
    procedure SaveGridLayout();
    procedure RestoreGridLayout();


  public
    { Public declarations }
  end;

var
  Main: TMain;

implementation
  uses
    frmData;
{$R *.dfm}

function TMain.GetColumn(aFieldName : string) : TcxGridDBColumn;
var
  i : integer;
begin
  for i := 0 to cxView.ColumnCount - 1 do
    if cxView.Columns[i].DataBinding.FieldName = aFieldName then
    begin
      Result := cxView.Columns[i];
      exit;
    end;
  Result := nil;
end;

procedure TMain.btnRestoreClick(Sender: TObject);
begin
  RestoreGridLayout();
end;

procedure TMain.btnSaveClick(Sender: TObject);
begin
  SaveGridLayout();
end;

procedure TMain.FormDestroy(Sender: TObject);
begin
  mt.Free;
end;

procedure TMain.FormShow(Sender: TObject);
begin
  RestoreGridLayout();
  btnExecute.OnClick(nil);
end;

procedure TMain.btnExecuteClick(Sender: TObject);
var
 selected: String;
begin
  if btnExecute.Caption = '����' then
  begin
    btnExecute.Caption:= '����';
    mt:=TMyThread.Create(10000, False);
  end
  else
  begin
    mt.Free;
    btnExecute.Caption:= '����';
  end;
end;

procedure TMain.RestoreGridLayout;
var
  ini_file: TIniFile;
  ini_file_name: String;
  field_name: String;
  i, index, width, group_index, sort_order, k: Integer;
  cnt, focused_index: Integer;
  sl: TStringList;
  column: TcxGridDBColumn;
  rec: TcxCustomGridRecord;
  groups: TArray<String>;
begin
  cxGrid.BeginUpdate();
  sl:= TStringList.Create;
  ini_file_name:= ChangeFileExt(ParamStr(0), '.ini');
  ini_file:= TIniFile.Create(ini_file_name);
  try
    ini_file.ReadSection('layout', sl);
    focused_index:= StrToIntDef(ini_file.ReadString('items', 'focused', '-1'), -1);

    for i := 0 to sl.Count -1 do
    begin
      field_name:= SplitString(sl[i], ';')[0];
      index:= StrToIntDef(SplitString(sl[i], ';')[1], 0);
      width:= StrToIntDef(SplitString(sl[i], ';')[2], 0);
      group_index:= StrToIntDef(SplitString(sl[i], ';')[3], -1);
      sort_order:= StrToIntDef(SplitString(sl[i], ';')[4], -1);
      column:= GetColumn(field_name);
      if column <> nil then
      begin
        column.Index:= index;
        column.Width:= width;
        column.GroupIndex:= group_index;
        column.Visible:= group_index < 0;
        if (group_index < 0) and (sort_order > 0) then
          column.SortOrder:= TdxSortOrder(sort_order);
      end;
    end;

    sl.Clear;
    ini_file.ReadSection('groups', sl);
    groups:= SplitString(sl.DelimitedText, ',');
    cxView.DataController.Groups.FullExpand;
    cnt:= cxView.ViewData.RecordCount;
    k:=0;
    for i := 0 to cnt -1 do
    begin
      rec:= cxView.ViewData.GetRecordByIndex(i);
      if (rec <> nil) and (not rec.IsData) then
      begin
        if MatchStr(IntToStr(k), groups) then
          rec.Expand(false)
        else
          rec.Collapse(False);
        inc(k);
      end;
    end;
    cxView.DataController.FocusedRecordIndex:= focused_index;
    cxGrid.SetFocus;
  finally
    ini_file.Free;
    sl.Free;
    cxGrid.EndUpdate();
  end;

end;

procedure TMain.SaveGridLayout;
var
  ini_file: TIniFile;
  ini_file_name: String;
  i, k: Integer;
begin
  ini_file_name:= ChangeFileExt(ParamStr(0), '.ini');
  ini_file:= TIniFile.Create(ini_file_name);
  try
    ini_file.EraseSection('layout');
    for i := 0 to cxView.ColumnCount -1 do
      ini_file.WriteString('layout', cxView.Columns[i].DataBinding.FieldName + ';' +
        IntToStr(i) + ';' + IntToStr(cxView.Columns[i].Width)+ ';' +
        IntToStr(cxView.Columns[i].GroupIndex)+ ';' +
        IntToStr(Integer(cxView.Columns[i].SortOrder)), '');

    ini_file.EraseSection('groups');
    cxGrid.BeginUpdate();
    try
      k:=0;
      for i:=0 to cxView.ViewData.RecordCount - 1 do
      begin
        if (not cxView.ViewData.GetRecordByIndex(i).IsData) then
        begin
          if TcxGridGroupRow(cxView.ViewData.GetRecordByIndex(i)).Expanded then
            ini_file.WriteString('groups', IntToStr(k), 'true');
          inc(k);
        end;
      end;
      ini_file.WriteString('items', 'focused', IntToStr(cxView.DataController.FocusedRecordIndex));

      cxGrid.SetFocus;
    finally
      cxGrid.EndUpdate();
    end;

  finally
    ini_file.Free;
  end;
end;

{ TMyThread }

constructor TMyThread.Create(Interval: Cardinal; CreateSuspended: Boolean);
begin
  inherited Create(CreateSuspended);
  FInterval := Interval;
  FWaitEvent := TEvent.Create(nil, False, False, '');
end;

destructor TMyThread.Destroy;
begin
  FWaitEvent.Free;
  inherited;
end;

procedure TMyThread.Execute;
var
  Msg: TMsg;
begin
  while not Terminated do
  begin
    Synchronize(Application.ProcessMessages);
    Synchronize(UpdateData);
    if wrSignaled = FWaitEvent.WaitFor(FInterval) then
      Break;
  end;
end;

procedure TMyThread.TerminatedSet;
begin
  inherited;
  FWaitEvent.SetEvent;
end;

procedure TMyThread.UpdateData;
var
  selected: String;
begin
  Main.cxGrid.BeginUpdate();
  dm.que.DisableControls;
  try
    selected:= dm.que.FieldByName('symbol').asString;
    dm.LoadData();
    dm.que.Locate('symbol', selected, []);
  finally
    dm.que.EnableControls;
    Main.cxGrid.EndUpdate();
  end;
end;

end.
