unit frmData;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, IdBaseComponent, IdComponent, IdTCPConnection, IdException,
  IdTCPClient, IdHTTP, System.JSON, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL, StrUtils;

type
  TCurrInfo = record
    symbol:       String;
    symbol1:       String;
    symbol2:       String;
    high:         Real;
    low:          Real;
    volume:       Real;
    quoteVolume:  Real;
    percentChange:Real;
    updatedAt:    String;
  end;

type
  Tdm = class(TDataModule)
    fdc: TFDConnection;
    fdt: TFDTransaction;
    que: TFDQuery;
    upd: TFDUpdateSQL;
    procedure fdcBeforeConnect(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    app_path: String;
    db_path: String;
    fs: TFormatSettings;
    function LoadRemoteData(var arr: TArray<TCurrInfo>): Boolean;
    function ParseJSON(const json: String; var arr: TArray<TCurrInfo>): Boolean;
    function SaveToDB(var arr: TArray<TCurrInfo>): Boolean;
  public
    function LoadData(): Boolean;
    procedure LoadDataArray(var arr: TArray<TCurrInfo>);
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure Tdm.DataModuleCreate(Sender: TObject);
begin
  fs:= TFormatSettings.Create;
  fs.DecimalSeparator:= '.';

  app_path:= ExtractFilePath(ParamStr(0));
  db_path:= app_path+ '/db/data.sqlite';
  que.DisableControls;
  try
    fdc.Open;
    que.Open;
    que.First;
  finally
    que.EnableControls;
  end;
end;

procedure Tdm.fdcBeforeConnect(Sender: TObject);
begin
  if not FileExists(db_path) then
    exit;
  fdc.Params.Database := db_path;
end;

function Tdm.LoadData: Boolean;
var
  arr: TArray<TCurrInfo>;
begin
  Result:= True;
  arr:= TArray<TCurrInfo>.Create();
  try
    if LoadRemoteData(arr) then
      Result:= SaveToDB(arr);
  finally
    arr:= nil;
  end;
end;

procedure Tdm.LoadDataArray(var arr: TArray<TCurrInfo>);
begin
  arr:= default(TArray<TCurrInfo>);
  que.DisableControls;
  try
    if LoadRemoteData(arr) then
      SaveToDB(arr);
  finally
    que.EnableControls;
  end;
end;

function Tdm.LoadRemoteData(var arr: TArray<TCurrInfo>): Boolean;
var
  idHTTP: TIdHTTP;
  IdSSL: TIdSSLIOHandlerSocketOpenSSL;
  sl: TStringList;
  res: String;
const
  url = 'https://api.bittrex.com/v3/markets/summaries';
begin
  Result:= False;
  idHTTP:= TIdHTTP.Create(self);
  IdHTTP.ConnectTimeout:=5000;
  IdHTTP.ReadTimeout:=5000;
  IdHTTP.AllowCookies := True;
  IdSSL := TIdSSLIOHandlerSocketOpenSSL.Create(IdHTTP);
  IdHTTP.IOHandler := IdSSL;
  IdSSL.SSLOptions.SSLVersions:= [sslvTLSv1_2];
  try
    try
      res := idHTTP.Get(url);
      Result:= ParseJSON(res, arr);
    except
      On E: EIdException do
      begin
        sl:= TStringList.Create;
        if FileExists(app_path+'\errors.txt') then
          sl.LoadFromFile(app_path+'\errors.txt');
        sl.Add(url+': '+ E.ToString);
        sl.SaveToFile(app_path+'\errors.txt');
        sl.Free;
      end;
    end;
  finally
    IdSSL.Free;
    idHttp.Free;
  end;

end;

function Tdm.ParseJSON(const json: String; var arr: TArray<TCurrInfo>): Boolean;
var
  jvi: TJSONObject;
  ja: TJSONArray;
  jv: TJSONValue;
  curr_info: TCurrInfo;
begin
  Result:= False;
  ja := TJSONObject.ParseJSONValue(json) as TJSONArray;
  try
    if ja = nil then
      exit;

    for jv in ja do
    begin
      jvi:= jv as TJSONObject;
      curr_info:= default(TCurrInfo);
      curr_info.symbol:= jvi.GetValue<String>('symbol');
      curr_info.symbol1:= SplitString(curr_info.symbol,'-')[0];
      curr_info.symbol2:= SplitString(curr_info.symbol,'-')[1];
      curr_info.high:= StrToFloat(jvi.GetValue<String>('high'), fs);
      curr_info.low:= StrToFloat(jvi.GetValue<String>('low'), fs);
      curr_info.volume:= StrToFloat(jvi.GetValue<String>('volume'), fs);
      if jvi.Get('quoteVolume')<>nil then
      curr_info.quoteVolume:= StrToFloat(jvi.GetValue<String>('quoteVolume'), fs);
      if jvi.Get('percentChange')<>nil then
        curr_info.percentChange:= StrToFloat(jvi.GetValue<String>('percentChange'), fs);

      curr_info.updatedAt:= jvi.GetValue<String>('updatedAt');
      arr:= arr + [curr_info];
    end;
    Result:= True;
  finally
    ja.Destroy;
  end;
end;

function Tdm.SaveToDB(var arr: TArray<TCurrInfo>): Boolean;
var
  ci: TCurrInfo;
  sql: String;
begin
  Result:= True;
  fdc.StartTransaction;
  try
    for ci in arr do
    begin
      if not que.Locate('symbol', ci.symbol, []) then
        que.Append
      else if (que.FieldByName('high').AsFloat <> ci.high) or
           (que.FieldByName('low').AsFloat <> ci.low) or
           (que.FieldByName('volume').AsFloat <> ci.volume) or
           (que.FieldByName('quoteVolume').AsFloat <> ci.quoteVolume) or
           (que.FieldByName('percentChange').AsFloat <> ci.percentChange) or
           (que.FieldByName('updatedAt').AsString <> ci.updatedAt) then
        que.Edit;

      if que.State in [dsEdit, dsInsert] then
      begin
        que.FieldByName('symbol').AsString:= ci.symbol;
        que.FieldByName('symbol1').AsString:= ci.symbol1;
        que.FieldByName('symbol2').AsString:= ci.symbol2;
        que.FieldByName('high').AsFloat:= ci.high;
        que.FieldByName('low').AsFloat:= ci.low;
        que.FieldByName('volume').AsFloat:= ci.volume;
        que.FieldByName('quoteVolume').AsFloat:= ci.quoteVolume;
        que.FieldByName('percentChange').AsFloat:= ci.percentChange;
        que.FieldByName('updatedAt').AsString:= ci.updatedAt;
        que.Post;
      end;
    end;
    fdc.Commit;
  except
    Result:= False;
    fdc.Rollback;
  end;

end;

end.
