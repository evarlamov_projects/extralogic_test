program test;

uses
  Vcl.Forms,
  frmMain in 'frmMain.pas' {Main},
  frmData in 'frmData.pas' {dm: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TMain, Main);
  Application.Run;
end.
