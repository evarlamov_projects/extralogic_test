object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 398
  Width = 492
  object fdc: TFDConnection
    Params.Strings = (
      'LockingMode=Normal'
      'DriverID=SQLite')
    LoginPrompt = False
    Transaction = fdt
    UpdateTransaction = fdt
    BeforeConnect = fdcBeforeConnect
    Left = 48
    Top = 24
  end
  object fdt: TFDTransaction
    Connection = fdc
    Left = 112
    Top = 24
  end
  object que: TFDQuery
    Connection = fdc
    UpdateObject = upd
    SQL.Strings = (
      'select m.* from markets m')
    Left = 192
    Top = 24
  end
  object upd: TFDUpdateSQL
    Connection = fdc
    InsertSQL.Strings = (
      'INSERT INTO MARKETS'
      '(SYMBOL, HIGH, LOW, VOLUME, QUOTEVOLUME, '
      '  PERCENTCHANGE, UPDATEDAT, SYMBOL1, SYMBOL2)'
      
        'VALUES (:NEW_symbol, :NEW_high, :NEW_low, :NEW_volume, :NEW_quot' +
        'eVolume, '
      
        '  :NEW_percentChange, :NEW_updatedAt, :NEW_symbol1, :NEW_symbol2' +
        ')')
    ModifySQL.Strings = (
      'UPDATE MARKETS'
      
        'SET SYMBOL = :NEW_symbol, HIGH = :NEW_high, LOW = :NEW_low, VOLU' +
        'ME = :NEW_volume, '
      
        '  QUOTEVOLUME = :NEW_quoteVolume, PERCENTCHANGE = :NEW_percentCh' +
        'ange, '
      
        '  UPDATEDAT = :NEW_updatedAt, SYMBOL1 = :NEW_symbol1, SYMBOL2 = ' +
        ':NEW_symbol2'
      'WHERE SYMBOL = :OLD_symbol')
    DeleteSQL.Strings = (
      'DELETE FROM MARKETS'
      'WHERE SYMBOL = :OLD_symbol')
    FetchRowSQL.Strings = (
      
        'SELECT SYMBOL, HIGH, LOW, VOLUME, QUOTEVOLUME, PERCENTCHANGE, UP' +
        'DATEDAT, '
      '  SYMBOL1, SYMBOL2'
      'FROM MARKETS'
      'WHERE SYMBOL = :OLD_symbol')
    Left = 256
    Top = 24
  end
end
