object Main: TMain
  Left = 0
  Top = 0
  Caption = 'Markets'
  ClientHeight = 598
  ClientWidth = 1076
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1076
    Height = 41
    Align = alTop
    TabOrder = 0
    object btnExecute: TButton
      Left = 10
      Top = 7
      Width = 75
      Height = 25
      Caption = #1055#1091#1089#1082
      TabOrder = 0
      OnClick = btnExecuteClick
    end
    object btnSave: TButton
      Left = 91
      Top = 7
      Width = 182
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1085#1072#1089#1090#1088#1086#1081#1082#1080
      TabOrder = 1
      OnClick = btnSaveClick
    end
    object btnRestore: TButton
      Left = 279
      Top = 7
      Width = 182
      Height = 25
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1072#1089#1090#1088#1086#1081#1082#1080
      TabOrder = 2
      OnClick = btnRestoreClick
    end
  end
  object cxGrid: TcxGrid
    Left = 0
    Top = 41
    Width = 1076
    Height = 557
    Align = alClient
    TabOrder = 1
    object cxView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsData
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxViewsymbol: TcxGridDBColumn
        DataBinding.FieldName = 'symbol'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
      object cxSymbol1: TcxGridDBColumn
        DataBinding.FieldName = 'symbol1'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
      object cxSymbol2: TcxGridDBColumn
        DataBinding.FieldName = 'symbol2'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
      end
      object cxViewhigh: TcxGridDBColumn
        DataBinding.FieldName = 'high'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
      object cxViewlow: TcxGridDBColumn
        DataBinding.FieldName = 'low'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
      object cxViewvolume: TcxGridDBColumn
        DataBinding.FieldName = 'volume'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
      object cxViewquoteVolume: TcxGridDBColumn
        DataBinding.FieldName = 'quoteVolume'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
      object cxViewpercentChange: TcxGridDBColumn
        DataBinding.FieldName = 'percentChange'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
      object cxViewupdatedAt: TcxGridDBColumn
        DataBinding.FieldName = 'updatedAt'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        VisibleForEditForm = bFalse
      end
    end
    object cxLevel: TcxGridLevel
      GridView = cxView
    end
  end
  object dsData: TDataSource
    DataSet = dm.que
    Left = 416
    Top = 120
  end
end
